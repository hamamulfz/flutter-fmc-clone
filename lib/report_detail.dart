import 'package:flutter/material.dart';
import 'dart:convert';
// import 'dart:async';
// import 'package:flutter/services.dart';

// class ReportTab extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: "List Search",
//       debugShowCheckedModeBanner: false,
//       home: MyReportDetail(title: "List"),
//     );
//   }
// }

class MyReportDetail extends StatefulWidget {
  MyReportDetail({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyReportDetailState createState() => _MyReportDetailState();
}

class _MyReportDetailState extends State<MyReportDetail> {
  List data;

  var jsonString =
      '{ "name": "Administrator", "password": "password", "picture": "no pic","token": "token","email": "admin@mail.com", "phone": "081212344321", "username": "admin", "jabatan": "Administrator"}';

  final popupMenu = new PopupMenuButton(
    child: new ListTile(
      title: new Text('Doge or lion?'),
      trailing: const Icon(Icons.more_vert),
    ),
    itemBuilder: (_) => <PopupMenuItem<String>>[
      new PopupMenuItem<String>(child: new Text('Doge'), value: 'Doge'),
      new PopupMenuItem<String>(child: new Text('Lion'), value: 'Lion'),
    ],
    onSelected: null,
  );

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    Map<String, dynamic> userData = jsonDecode(jsonString);

    String nama = userData['username'],
        email = userData['email'],
        jabatan = userData['jabatan'];

    return new Scaffold(
      appBar: AppBar(
        title: Text("Report Detail"),
      ),
      body: Stack(children: <Widget>[
        Center(
          child: new Image.asset(
            "assets/back.png",
            width: size.width,
            height: size.height,
            fit: BoxFit.fill,
          ),
        ),
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Card(
                color: Colors.transparent,
                elevation: 0,
                shape: new RoundedRectangleBorder(
                    side: new BorderSide(color: Colors.white, width: 2.0),
                    borderRadius: BorderRadius.circular(5.0)),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            nama,
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            email,
                            style: TextStyle(color: Colors.white),
                          ),
                          Text(
                            jabatan,
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('assets/report_detail.json'),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return CircularProgressIndicator();
                  }
                  var report = json.decode(snapshot.data);
                  // var data = myData['data'];
                  print(report);

                  String _kategori = report['category_name'],
                      _subKategori = report['sub_name'],
                      _daop = report['daop'],
                      _stasiun = report['stasiun'],
                      _noOrder = report['no_order'],
                      _tanggalOrder = report['tgl_order'];
                  var data = report['data']['detail'];

                  print(data);

                  return Column(
                    children: <Widget>[
                      Card(
                        color: Colors.transparent,
                        elevation: 0,
                        shape: new RoundedRectangleBorder(
                            side:
                                new BorderSide(color: Colors.white, width: 2.0),
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  expandedRow(
                                      textView(
                                          "Kategori", Alignment.centerLeft),
                                      "text",
                                      2),
                                  expandedRow(
                                      textView("Sub Kategori",
                                          Alignment.centerRight),
                                      "text",
                                      2),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    expandedRow(
                                        textView(
                                            _kategori, Alignment.centerLeft),
                                        "text",
                                        2),
                                    expandedRow(
                                        textView(_subKategori,
                                            Alignment.centerRight),
                                        "text",
                                        2),
                                  ],
                                ),
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  expandedRow(
                                      textView(
                                          "Daop/Divre", Alignment.centerLeft),
                                      "text",
                                      2),
                                  expandedRow(
                                      textView(
                                          "Stasiun", Alignment.centerRight),
                                      "text",
                                      2),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    expandedRow(
                                        textView(_daop, Alignment.centerLeft),
                                        "text",
                                        2),
                                    expandedRow(
                                        textView(
                                            _stasiun, Alignment.centerRight),
                                        "text",
                                        2),
                                  ],
                                ),
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  expandedRow(
                                      textView(
                                          "No Order", Alignment.centerLeft),
                                      "text",
                                      2),
                                  expandedRow(
                                      textView("Tanggal Order",
                                          Alignment.centerRight),
                                      "text",
                                      2),
                                ],
                              ),
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  expandedRow(
                                      textView(_noOrder, Alignment.centerLeft),
                                      "text",
                                      2),
                                  expandedRow(
                                      textView(
                                          _tanggalOrder, Alignment.centerRight),
                                      "text",
                                      2),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      _buildChild(data)
                    ],
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Material(
                  elevation: 5.0,
                  // borderRadius: BorderRadius.circular(15.0),
                  color: Color(0xff01A0C7),
                  child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () {
                        Navigator.pop(
                          context,
                        );
                      },
                      child: Text(
                        "Close",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      )),
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }

  ListView _buildChild(json) {
    return ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: json.length == 0 ? 0 : json.length,
        itemBuilder: (context, index) {
          int string = json[index]['ispenilaian'];
          print('$string');
          if (string == 1) {
            return Container(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  expandedRow(
                      textView('${json[index]['component_name']}',
                          Alignment.centerLeft),
                      "non",
                      8),
                  expandedRow(
                      buttonSkala(json[index]),
                      // textView(
                      //     '${json[index]['value_skala']}', Alignment.centerRight),
                      "non",
                      2),
                ],
              ),
            );
          } else {
            var subkom = json[index]['sub_component'];
            return Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: textView('${json[index]['component_name']}',
                        Alignment.centerLeft),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child:
                        // (subkom[index]).map((answer) {
                        //   return Container(
                        //     child: Row(
                        //       children: <Widget>[
                        //         expandedRow(
                        //             textView('${answer['sub_component_name']}',
                        //                 Alignment.centerLeft),
                        //             "non",
                        //             8),
                        //         expandedRow(
                        //             // textView('${subkom[index]['value_skala']}',
                        //             //     Alignment.centerRight),
                        //             buttonSkala(answer),
                        //             "non",
                        //             2),
                        //       ],
                        //     ),
                        //   );
                        // }).toList(),

                        ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: subkom.length == 0 ? 0 : subkom.length,
                      itemBuilder: (context, index) {
                        return Container(
                          child: Row(
                            children: <Widget>[
                              expandedRow(
                                  textView(
                                      '${subkom[index]['sub_component_name']}',
                                      Alignment.centerLeft),
                                  "non",
                                  8),
                              expandedRow(
                                  // textView('${subkom[index]['value_skala']}',
                                  //     Alignment.centerRight),
                                  buttonSkala(json[index]),
                                  "non",
                                  2),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }
        });
    // if (json['ispenilaian']==0) {
//       var komponen = json['component_name'];
// var subKomponen = json['sub_component']['sub_component_name'];
// var skala = json['sub_component']['value_skala'];
// print(komponen + subKomponen + skala);
    // return Text("Haha");
    // Row(
    //   mainAxisSize: MainAxisSize.max,
    //   children: <Widget>[
    //     expandedRow(textView("$komponen", Alignment.centerLeft),
    //         "text"),
    //     Row(
    //       mainAxisSize: MainAxisSize.max,
    //       children: <Widget>[
    //         expandedRow(
    //             textView("$subKomponen",
    //                 Alignment.centerLeft),
    //             "text"),
    //         expandedRow(
    //             textView("$skala",
    //                 Alignment.centerRight),
    //             "text"),
    //       ],
    //     ),
    //   ],
    // );
    // } else {
    // var komponen = json['component_name'];
    // var skala = json['value_skala'];
    // return Text("Hoho");
    //  Row(
    //   mainAxisSize: MainAxisSize.max,
    //   children: <Widget>[
    //     expandedRow(textView("$komponen", Alignment.centerLeft),
    //         "text"),
    //     expandedRow(
    //         textView("$skala", Alignment.centerRight), "text"),
    //   ],
    // );
    // }
  }

  Widget textView(String title, Alignment align) {
    final textWidget = new Align(
      alignment: align,
      child: new Text(
        title,
        style: TextStyle(color: Colors.white),
      ),
    );
    return textWidget;
  }

  Widget expandedRow(Widget widget, String type, int flex) {
    // int flexA = flex;
    return Expanded(
      flex: flex,
      child: Container(
        // color: Colors.blue,
        child: new SizedBox(
          width: double.infinity,

          // height: double.infinity,
          child: Padding(
            padding: type == "text"
                ? const EdgeInsets.only(left: 8.0, right: 8.0, top: 0.0)
                : const EdgeInsets.only(left: 8.0, right: 8.0),
            child: widget,
          ),
        ),
      ),
    );
  }

  Widget buttonSkala(json) {
    return MaterialButton(
      shape: new RoundedRectangleBorder(
        side: new BorderSide(color: Colors.white, width: 2.0),
        // borderRadius: BorderRadius.circular(5.0)
      ),
      onPressed: () {
        _showDialog(json);
      },
      child: textView('${json['value_skala']} ', Alignment.center),
    );
  }

// user defined function
  void _showDialog(json) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Skala Penilaian"),
          content: new Text('${json['value_skala']} - ${json['skala_name']}'),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
