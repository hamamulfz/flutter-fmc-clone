import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:hello/list_sl.dart';
import './buttomnav/tab/tab_report.dart';

import './buttomnav/buttom_nav.dart';
// import 'dart:async';
// import 'package:flutter/services.dart';

// class ReportTab extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: "List Search",
//       debugShowCheckedModeBanner: false,
//       home: MyReportDetail(title: "List"),
//     );
//   }
// }

class MyAssessment extends StatefulWidget {
  MyAssessment({Key key, this.title, this.list}) : super(key: key);
  final String title;
  final List list;
  @override
  _MyAssessmentState createState() => _MyAssessmentState();
}

class _MyAssessmentState extends State<MyAssessment> {
  List data;

  var jsonString =
      '{ "name": "Administrator", "password": "password", "picture": "no pic","token": "token","email": "admin@mail.com", "phone": "081212344321", "username": "admin", "jabatan": "Administrator"}';
  var skala = [];
  Map jsonSkala;
  // var data;

  // _navigateToSelectSl(var jsonSl, BuildContext context) async {

  // }

  _changeSkalaValue(int parent, int value) {
    setState(() {
      data[parent]['value_skala'] = value;
      print(value);
      skala[parent] = value;
      print(data[parent]);
      print(skala);
    });
  }

  _confirmOnsubmit(BuildContext context) async {
    final result = await _showDialog();
    if (result != null) {
      print(result);
      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ReportTab(),
                        ),
                      );
    }
  }
  
// user defined function
   _showDialog() {
    // flutter defined function
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Alert"),
          content: Text("Apakah anda yakin ingin mengirimkan data?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Kembali"),
              onPressed: () {
                Navigator.pop(context, false);
              },
            ),
            new FlatButton(
              child: new Text("Kirim"),
              onPressed: ()  {
                Navigator.pop(context, true);
              },
            ),
          ],
        );
      },
    );
  }

  Widget buttonSkala(json, BuildContext context, int index) {
    // var thisSkala;
    // int index = 0;
    // if(skala.length <= json){
    skala.add(json['value_skala']);
    // }
    return MaterialButton(
      shape: new RoundedRectangleBorder(
        side: new BorderSide(color: Colors.white, width: 2.0),
        // borderRadius: BorderRadius.circular(5.0)
      ),
      onPressed: () async {
        final result = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MySlList(
              skalaLikert: json['skala_likert'],
            ),
          ),
        );
        if (result != null) {
          // jsonSkala = result;
          print('balikan $result');

          // newSkala = result;
          // jsonSkala['skala'];
          // print(skala);
          _changeSkalaValue(index, result);
          // print(_kategori);
        }
        // _navigateToSelectSl(json['skala_likert'], context);
        // print(json);
        // var action =  _asyncSimpleDialog(context, json['skala_likert']);
        // print("Confirm Action $action" );
      },
      // _showDialog(json);

      child: textView('${skala[index]}', Alignment.center),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    Map<String, dynamic> userData = jsonDecode(jsonString);

    String nama = userData['username'],
        email = userData['email'],
        jabatan = userData['jabatan'];

    return new Scaffold(
      appBar: AppBar(
        title: Text("Checksheet"),
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: new Image.asset(
              "assets/back.png",
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
          ),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Card(
                  color: Colors.transparent,
                  elevation: 0,
                  shape: new RoundedRectangleBorder(
                      side: new BorderSide(color: Colors.white, width: 2.0),
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              nama,
                              style: TextStyle(color: Colors.white),
                            ),
                            Text(
                              email,
                              style: TextStyle(color: Colors.white),
                            ),
                            Text(
                              jabatan,
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                FutureBuilder(
                  future: DefaultAssetBundle.of(context)
                      .loadString('assets/checksheet.json'),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return CircularProgressIndicator();
                    }
                    var report = json.decode(snapshot.data);

                    // String
                        // _kategori = 'report',
                        // _subKategori = 'sub_name',
                        // _daop = 'daop',
                        // _stasiun = 'stasiun',
                        // _noOrder = 'AUTO GENERATED'
                        // _tanggalOrder = 'tgl_order'
                        // ;
                    data = report['data']['detail'];

                    // print(data);

                    return Column(
                      children: <Widget>[
                        Card(
                          color: Colors.transparent,
                          elevation: 0,
                          shape: new RoundedRectangleBorder(
                              side: new BorderSide(
                                  color: Colors.white, width: 2.0),
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    expandedRow(
                                        textView(
                                            "Kategori", Alignment.centerLeft),
                                        "text",
                                        2),
                                    expandedRow(
                                        textView("Sub Kategori",
                                            Alignment.centerRight),
                                        "text",
                                        2),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 8.0),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      expandedRow(
                                          textView(widget.list[0],
                                              Alignment.centerLeft),
                                          "text",
                                          2),
                                      expandedRow(
                                          textView(widget.list[1],
                                              Alignment.centerRight),
                                          "text",
                                          2),
                                    ],
                                  ),
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    expandedRow(
                                        textView(
                                            "Daop/Divre", Alignment.centerLeft),
                                        "text",
                                        2),
                                    expandedRow(
                                        textView(
                                            "Stasiun", Alignment.centerRight),
                                        "text",
                                        2),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 8.0),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      expandedRow(
                                          textView(widget.list[2],
                                              Alignment.centerLeft),
                                          "text",
                                          2),
                                      expandedRow(
                                          textView(widget.list[3],
                                              Alignment.centerRight),
                                          "text",
                                          2),
                                    ],
                                  ),
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    expandedRow(
                                        textView(
                                            "No Order", Alignment.centerLeft),
                                        "text",
                                        2),
                                    expandedRow(
                                        textView("Tanggal Order",
                                            Alignment.centerRight),
                                        "text",
                                        2),
                                  ],
                                ),
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    expandedRow(
                                        textView(
                                            "AUTO GENERATED", Alignment.centerLeft),
                                        "text",
                                        2),
                                    expandedRow(
                                        textView(widget.list[5],
                                            Alignment.centerRight),
                                        "text",
                                        2),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        _buildChild(data)
                      ],
                    );
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 5.0,
                    color: Color(0xff01A0C7),
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () async {
                        _confirmOnsubmit(context);
                        // _showDialog();
                        // Navigator.pushNamed(context, '/navbar');
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) => MySearchList()),
                        // );
                      },
                      child: Text(
                        "Submit",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ListView _buildChild(json) {
    return ListView.builder(
      
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: json.length == 0 ? 0 : json.length,
        itemBuilder: (context, index) {
          int string = json[index]['ispenilaian'];
          // print('$string');
          if (string == 1) {
            return Container(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  expandedRow(
                      textView('${json[index]['component_name']}',
                          Alignment.centerLeft),
                      "non",
                      8),
                  expandedRow(
                      buttonSkala(json[index], context, index),
                      // textView(
                      //     '${json[index]['value_skala']}', Alignment.centerRight),
                      "non",
                      2),
                ],
              ),
            );
          } else {
            var subkom = json[index]['sub_component'];
            // print("subkom" + subkom);
            return Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: textView('${json[index]['component_name']}',
                        Alignment.centerLeft),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: ListView.builder(
                      
        physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: subkom.length == 0 ? 0 : subkom.length,
                      itemBuilder: (context, index) {
                        return Container(
                          child: Row(
                            children: <Widget>[
                              expandedRow(
                                  textView(
                                      '${subkom[index]['sub_component_name']}',
                                      Alignment.centerLeft),
                                  "non",
                                  8),
                              expandedRow(
                                  // textView('${subkom[index]['value_skala']}',
                                  //     Alignment.centerRight),
                                  buttonSkala(json[index], context, index),
                                  "non",
                                  2),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }
        });
  }

  Widget textView(String title, Alignment align) {
    final textWidget = new Align(
      alignment: align,
      child: new Text(
        title,
        style: TextStyle(color: Colors.white),
      ),
    );
    return textWidget;
  }

  Widget expandedRow(Widget widget, String type, int flex) {
    // int flexA = flex;
    return Expanded(
      flex: flex,
      child: Container(
        // color: Colors.blue,
        child: new SizedBox(
          width: double.infinity,

          // height: double.infinity,
          child: Padding(
            padding: type == "text"
                ? const EdgeInsets.only(left: 8.0, right: 8.0, top: 0.0)
                : const EdgeInsets.only(left: 8.0, right: 8.0),
            child: widget,
          ),
        ),
      ),
    );
  }


  showAlertDialog(BuildContext assesmentContext) {
    // set up the buttons
    // Widget cancelButton =
    Widget continueButton = FlatButton(
      child: Text("Kirim"),
      onPressed: () {
        Navigator.pop(assesmentContext, true);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Apakah anda yakin ingin mengirimkan data?"),
      actions: [
        FlatButton(
          child: Text("Kembali"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: assesmentContext,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

// // user defined function
//   void _showDialog(json) {
//     // flutter defined function
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         // return object of type Dialog
//         return AlertDialog(
//           title: new Text("Skala Penilaian"),
//           content: new Text('${json['value_skala']} - ${json['skala_name']}'),
//           actions: <Widget>[
//             // usually buttons at the bottom of the dialog
//             new FlatButton(
//               child: new Text("Close"),
//               onPressed: () {
//                 Navigator.of(context).pop();
//               },
//             ),
//           ],
//         );
//       },
//     );
//   }
}
