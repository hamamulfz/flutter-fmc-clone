import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
// import 'package:flutter/semantics.dart';
// import 'dart:convert';
// import 'package:flutter/services.dart' show rootBundle;

// void main() {
//   runApp(SearchList());
// }

// class SearchList extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: "List Search",
//       home: MySearchList(title: "List"),
//     );
//   }
// }

class MySlList extends StatefulWidget {
  MySlList({Key key, this.title, this.skalaLikert}) : super(key: key);
  final String title;
  final skalaLikert;
  @override
  _MySlListState createState() => _MySlListState();
}

class _MySlListState extends State<MySlList> {
  // MyApp({Key key, @required this.list}) : super(key: key);
  TextEditingController searchNameController = TextEditingController();
  TextEditingController searchNoController = TextEditingController();

  // final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  var items;
  @override
  void initState() {
    // items.addAll(duplicateItems);
    super.initState();
  }

  void filterSearchResults(String query, var duplicateItems) {
    List<Map> dummySearchList;
    dummySearchList.addAll(duplicateItems);
    print(duplicateItems);
    if (query.isNotEmpty) {
      var dummyListData;
      // var i=0;
      print(query);
      dummySearchList.forEach((item) {
        if ((item['ka_nm']).contains(query)) {
          dummyListData.add(item);
        }
      });

      // dummySearchList.map(f){
      //    if(duplicateItems[i]['ka_nm'].contains(query)){
      // // print(duplicateItems[i]['ka_nm']);
      // dummyListData.add(duplicateItems[i]);
      // }
      // };
      // while (i < duplicateItems.length){
      //   if(duplicateItems[i]['ka_nm'].contains(query)){
      //     // print(duplicateItems[i]['ka_nm']);
      //     dummyListData.add(duplicateItems[i]);
      //   }
      // }
      // dummySearchList.forEach((item) {
      //   if (item.contains(query)) {
      //     dummyListData.add(item);
      //   }
      // });
      setState(() {
        items.clear();
        items = dummyListData;
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    items = widget.skalaLikert;
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Skala Penilaian"),
      ),
      body: Container(
        // height: double.infinity,
        // width: double.infinity,
        child: Column(
                children: <Widget>[
                  // Padding(
                  //   padding: const EdgeInsets.all(8.0),
                  //   child: TextField(
                  //     onChanged: (value) {
                  //       filterSearchResults(value, category);
                  //     },
                  //     controller: searchController,
                  //     decoration: InputDecoration(
                  //         labelText: "Search",
                  //         hintText: "Search",
                  //         prefixIcon: Icon(Icons.search),
                  //         border: OutlineInputBorder(
                  //             borderRadius:
                  //                 BorderRadius.all(Radius.circular(25.0)))),
                  //   ),
                  // ),
                  ListView.builder(
                      shrinkWrap: true,
                      itemCount: items.length == 0 ? 0 : items.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          onTap: () {
                            // Close the screen and return "Yep!" as the result.
                            Navigator.pop(context, items[index]['skala']);
                          },
                          title: Text(
                              '${items[index]['skala']} - ${items[index]['deskripsi']}'),
                        );
                      },
                    ),
                  
                ],
              ),
      ),
      // Column(
      //   children: <Widget>[
      //     ListView.builder(
      //       shrinkWrap: true,
      //       itemCount: items.length == 0 ? 0 : items.length,
      //       itemBuilder: (context, index) {
      //         return ListTile(
      //           onTap: () {
      //             // Close the screen and return "Yep!" as the result.
      //             Navigator.pop(context, '${items[index]}');
      //           },
      //           title: Text(
      //               '${items[index]['skala']} - ${items[index]['deskripsi']}'),
      //         );
      //       },
      //     ),
      //   ],
      // ),
    );
  }
}
