import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
// import 'package:flutter/semantics.dart';
import 'dart:convert';
// import 'package:flutter/services.dart' show rootBundle;

// void main() {
//   runApp(SearchList());
// }

// class SearchList extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: "List Search",
//       home: MySearchList(title: "List"),
//     );
//   }
// }

class MySearchList extends StatefulWidget {
  MySearchList({Key key, this.title, this.subkategory}) : super(key: key);
  final String title;
  final subkategory;
  @override
  _MySearchListState createState() => _MySearchListState();
}

class _MySearchListState extends State<MySearchList> {
  // MyApp({Key key, @required this.list}) : super(key: key);
  TextEditingController searchController = TextEditingController();

  // final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  var items;
  @override
  void initState() {
    // items.addAll(duplicateItems);
    super.initState();
  }

  void filterSearchResults(String query, var duplicateItems) {
    Map<String, dynamic> dummySearchList;
    dummySearchList.addAll(duplicateItems);
    print(duplicateItems);
    if (query.isNotEmpty) {
      var dummyListData ;
      var i=0;
          print(query);
      while (i < duplicateItems.length){
        if(duplicateItems[i]['category_name'].contains(query)){
          print(duplicateItems[i]['category_name']);
          dummyListData.add(duplicateItems[i]);
        }
      }
      // dummySearchList.forEach((item) {
      //   if (item.contains(query)) {
      //     dummyListData.add(item);
      //   }
      // });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("List"),
      ),
      body: Container(
        // height: double.infinity,
        // width: double.infinity,
        child: FutureBuilder(
            future: DefaultAssetBundle.of(context)
                .loadString('assets/data_json/category.json'),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return CircularProgressIndicator();
              }
              var category = json.decode(snapshot.data);
              items = category['data'];

              if(widget.subkategory!= null){
                items = items[widget.subkategory]['sub_category'];
              }
              return Column(
                children: <Widget>[
                  // Padding(
                  //   padding: const EdgeInsets.all(8.0),
                  //   child: TextField(
                  //     onChanged: (value) {
                  //       filterSearchResults(value, category);
                  //     },
                  //     controller: searchController,
                  //     decoration: InputDecoration(
                  //         labelText: "Search",
                  //         hintText: "Search",
                  //         prefixIcon: Icon(Icons.search),
                  //         border: OutlineInputBorder(
                  //             borderRadius:
                  //                 BorderRadius.all(Radius.circular(25.0)))),
                  //   ),
                  // ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: items.length == 0 ? 0 : items.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          onTap: () {
                            // Close the screen and return "Yep!" as the result.
                            Navigator.pop(
                                context, items[index]);
                          },
                          title: (widget.subkategory != null) ?Text('${items[index]['sub_name']}') : Text('${items[index]['category_name']}'),
                        );
                      },
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }
}
