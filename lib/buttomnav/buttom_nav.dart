import 'package:flutter/material.dart';
import 'package:hello/buttomnav/tab/tab_dashboard.dart';
import 'package:hello/buttomnav/tab/tab_order.dart';
import 'package:hello/buttomnav/tab/tab_report.dart';

// void main() {
//   runApp(ButtomNav());
// }

class ButtomNav extends StatelessWidget {
  final Map<String, dynamic> userData;

  ButtomNav({Key key, this.userData}): super(key: key);

  @override
  Widget build(BuildContext context) {
     var globalContext = context;
    return MaterialApp(
      title: "Buttom Navigation",
      debugShowCheckedModeBanner: false,
      home: MyButtomNavigation(userData: this.userData, parent: globalContext,),
    );
  }
}

class MyButtomNavigation extends StatefulWidget {
  
  final Map<String, dynamic> userData;
  final BuildContext parent;
  MyButtomNavigation({Key key, @required this.userData, this.parent }) : super(key: key);

  @override
  _MyButtomNavigationState createState() => _MyButtomNavigationState();
}

class _MyButtomNavigationState extends State<MyButtomNavigation>
    with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: TabBarView(
          children: <Widget>[DashboardTab(parentContext: widget.parent,), OrderTab(userData: widget.userData, parent: widget.parent,), ReportTab(parent: widget.parent)],
          controller: controller,
        ),
        bottomNavigationBar: Material(
          color: Color.fromRGBO(255, 255, 255, 0.5),
          child: TabBar(
            tabs: <Tab>[
              Tab(
                icon: Icon(Icons.home),
                text: "Home",
              ),
              Tab(
                icon: Icon(Icons.tab),
                text: "Order",
              ),
              Tab(
                icon: Icon(Icons.book),
                text: "Report",
              )
            ],
            controller: controller,
            labelColor: Colors.orange,
            unselectedLabelColor: Colors.black,
          ),
        ));
  }
}
