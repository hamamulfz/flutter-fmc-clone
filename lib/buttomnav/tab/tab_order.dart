import 'package:flutter/material.dart';
import '../../list_category.dart';
import '../../list_daop.dart';
import '../../list_ka.dart';
import 'package:intl/intl.dart';
import '../../assessment.dart';
import '../tab/tab_report.dart';

class OrderTab extends StatelessWidget {
  final Map<String, dynamic> userData;
  final BuildContext parent;
  OrderTab({Key key, @required this.userData, this.parent}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "List Search",
      debugShowCheckedModeBanner: false,
      home: MyOrder(
        title: "List",
        userData: this.userData,
        parent: this.parent,
      ),
    );
  }
}

class MyOrder extends StatefulWidget {
  final Map<String, dynamic> userData;

  final BuildContext parent;
  MyOrder({
    Key key,
    this.title,
    @required this.userData,
    this.parent,
  }) : super(key: key);
  final String title;

  @override
  _MyOrderState createState() => _MyOrderState();
}

class _MyOrderState extends State<MyOrder> {
  List data;
  String _kategori, _subkategori, _daop, _stasiun, asal, tujuan, _ka, stringDate;
  Map jsonCategory, jsonSubkategory, jsonDaop, jsonStasiun, jsonKa;
  // mitra,
  // sarana

// user defined function
  void _showDialog(String title, String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _navigateToSelectCategory(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MySearchList()),
    );
    setState(
      () {
        if (result != null) {
          jsonCategory = result;
          _kategori = jsonCategory['category_name'];
          print(result);

          // print(_kategori);
        }
      },
    );
  }

  _navigateToSelectSubCategory(BuildContext context) async {
    if (_kategori != null) {
      final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MySearchList(
            subkategory: jsonCategory['category_id'],
          ),
        ),
      );

      if (result != null) {
        setState(
          () {
            jsonSubkategory = result;
            _subkategori = jsonSubkategory['sub_name'];
          },
        );
      }
    } else {
      _showDialog("Alert", "Pilih Kategori Telebih Dahulu");
    }
    // print(subkategori);
  }

  _navigateToSelectDaop(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MyDaopList(),
      ),
    );

    if (result != null) {
      setState(
        () {
          jsonDaop = result;
          _daop = jsonDaop['singkatan'];
        },
      );
    }
  }

  _navigateToSelectStasiun(BuildContext context) async {
    if (_daop != null) {
      final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MyDaopList(
            daop: jsonDaop['id'],
          ),
        ),
      );
      if (result != null) {
        setState(
          () {
            jsonStasiun = result;
            _stasiun = jsonStasiun['nama_stn'];
          },
        );
      }
    } else {
      _showDialog("Alert", "Pilih Daop Telebih Dahulu");
    }
  }

  _navigateToSelectKa(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MyKaList(),
      ),
    );
    if (result != null) {
      setState(
        () {
          jsonKa = result;
          _ka = jsonKa['ka_nm'];
          asal = jsonKa['st_asal'];
          tujuan = jsonKa['st_tujuan'];
        },
      );
    }
  }

  _navigateToChecksheet(BuildContext context) async {
    String _errKategory, _errSubKategory, _errDaop, _errStasiun, _errKA;
    _errKategory = _kategori == null ?   "Pilih Kategori Telebih dahulu\n" : "";
    _errSubKategory =_subkategori == null
        ?  "Pilih Subkategori Telebih dahulu\n"
        : "";
    _daop == null ? _errDaop = "Pilih Daop Telebih dahulu\n" :  _errDaop ="";
    _stasiun == null ? _errStasiun = "Pilih Stasiun Telebih dahulu\n" : _errStasiun="";
    _ka == null ? _errKA = "Pilih KA Telebih dahulu\n" : _errKA="";

    if (_kategori == null ||
        _subkategori == null ||
        _daop == null ||
        _stasiun == null ||
        _ka == null) {
          
      _showDialog("Alert",
          _errKategory + _errSubKategory + _errDaop + _errStasiun + _errKA);
    } else {
      
      final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MyAssessment(
                  list: [_kategori, _subkategori, _daop, _stasiun, _ka, stringDate ],
                )),
      );

      if(result!= null && result){
        Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ReportTab()));
      }
    }
  }

  // _navigateToSelectAsal(BuildContext context) async {
  //   final result = await Navigator.push(
  //     context,
  //     MaterialPageRoute(
  //       builder: (context) => MySearchList(),
  //     ),
  //   );
  //   setState(
  //     () {
  //       asal = result;
  //     },
  //   );
  // }

  // _navigateToSelectTujuan(BuildContext context) async {
  //   final result = await Navigator.push(
  //     context,
  //     MaterialPageRoute(
  //       builder: (context) => MySearchList(),
  //     ),
  //   );
  //   setState(
  //     () {
  //       tujuan = result;
  //     },
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    String nama = widget.userData['username'],
        email = widget.userData['email'],
        jabatan = widget.userData['jabatan'];
    // print( widget.userData['jabatan']);

    final buttonKategori = MaterialButton(
      shape: new RoundedRectangleBorder(
        side: new BorderSide(color: Colors.white, width: 2.0),
        // borderRadius: BorderRadius.circular(5.0)
      ),
      onPressed: () {
        _navigateToSelectCategory(context);
      },
      child: textView(
          jsonCategory == null ? "Kategori" : jsonCategory['category_name'],
          Alignment.topLeft),
    );

    final buttonSubKategori = MaterialButton(
      shape: new RoundedRectangleBorder(
        side: new BorderSide(color: Colors.white, width: 2.0),
        // borderRadius: BorderRadius.circular(5.0)
      ),
      onPressed: () {
        _navigateToSelectSubCategory(context);
      },
      child: textView(_subkategori == null ? "SubKategori" : _subkategori,
          Alignment.topRight),
    );

    final buttonDaop = MaterialButton(
      shape: new RoundedRectangleBorder(
        side: new BorderSide(color: Colors.white, width: 2.0),
        // borderRadius: BorderRadius.circular(5.0)
      ),
      onPressed: () {
        _navigateToSelectDaop(context);
      },
      child: textView(_daop == null ? "Daop/Divre" : _daop, Alignment.topLeft),
    );

    final buttonStasiun = MaterialButton(
      shape: new RoundedRectangleBorder(
        side: new BorderSide(color: Colors.white, width: 2.0),
        // borderRadius: BorderRadius.circular(5.0)
      ),
      onPressed: () {
        _navigateToSelectStasiun(context);
      },
      child:
          textView(_stasiun == null ? "Stasiun" : _stasiun, Alignment.topRight),
    );

    final fieldSarana = TextField(
      // obscureText: true,
      style: TextStyle(color: Colors.white),
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Sarana",
        hintStyle: TextStyle(color: Colors.white),
        fillColor: Colors.white,
      ),
    );

    final fieldMitra = TextField(
      // obscureText: true,
      style: TextStyle(color: Colors.white),
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Mitra",
        hintStyle: TextStyle(color: Colors.white),
        fillColor: Colors.white,
      ),
    );

    DateTime selectedDate = DateTime.now();
     stringDate = DateFormat('yyyy-MM-dd').format(selectedDate);

    Future<Null> _selectDate(BuildContext context) async {
      final DateTime picked = await showDatePicker(
          context: context,
          initialDate: selectedDate,
          firstDate: DateTime(2015, 8),
          lastDate: DateTime(2101));
      if (picked != null)
        setState(
          () {
            stringDate = DateFormat('yyyy-mm-dd').format(picked);
          },
        );
    }

    Widget _buttonDate(BuildContext context) {
      return MaterialButton(
        shape: new RoundedRectangleBorder(
          side: new BorderSide(color: Colors.white, width: 2.0),
          // borderRadius: BorderRadius.circular(5.0)
        ),
        onPressed: () {
          _selectDate(context);
        },
        child: textView(stringDate == null ? "Select Date" : '$stringDate',
            Alignment.topRight),
      );
    }

    final buttonNo = MaterialButton(
      shape: new RoundedRectangleBorder(
        side: new BorderSide(color: Colors.white, width: 2.0),
        // borderRadius: BorderRadius.circular(5.0)
      ),
      onPressed: () {
        _navigateToSelectKa(context);
      },
      child: textView(_ka == null ? "No KA" : _ka, Alignment.topLeft),
    );

    final buttonRelasiAwal = MaterialButton(
        shape: new RoundedRectangleBorder(
          side: new BorderSide(color: Colors.white, width: 2.0),
          // borderRadius: BorderRadius.circular(5.0)
        ),
        onPressed: null,
        // () {
        // _navigateToSelectAsal(context);
        // },
        child: textView(asal == null ? "St. Asal" : asal, Alignment.center));

    final buttonRelasiAkhir = MaterialButton(
        shape: new RoundedRectangleBorder(
          side: new BorderSide(color: Colors.white, width: 2.0),
          // borderRadius: BorderRadius.circular(5.0)
        ),
        onPressed: null,
        // () {
        //   _navigateToSelectTujuan(context);
        // },
        child:
            textView(tujuan == null ? "St. Akhir" : tujuan, Alignment.center));

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Order Screen"),
        backgroundColor: Colors.blue,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              Navigator.pop(widget.parent);
            },
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: new Image.asset(
              "assets/back.png",
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
          ),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Card(
                  color: Colors.transparent,
                  elevation: 0,
                  shape: new RoundedRectangleBorder(
                      side: new BorderSide(color: Colors.white, width: 2.0),
                      borderRadius: BorderRadius.circular(5.0)),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              nama == null ? "Nama" : nama,
                              style: TextStyle(color: Colors.white),
                            ),
                            Text(
                              email == null ? "Email" : email,
                              style: TextStyle(color: Colors.white),
                            ),
                            Text(
                              jabatan == null ? "Jabatan" : jabatan,
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                // Card(
                //   color: Colors.transparent,
                //   elevation: 0,
                //   shape: new RoundedRectangleBorder(
                //       side: new BorderSide(color: Colors.white, width: 2.0),
                //       borderRadius: BorderRadius.circular(5.0)),
                //   child:
                // Padding(
                //     padding: const EdgeInsets.all(8.0),
                //     child:
                Column(
                  children: <Widget>[
                    // Row(
                    //   mainAxisSize: MainAxisSize.max,
                    //   children: <Widget>[
                    //     expandedRow(
                    //         textView("Kategori", Alignment.centerLeft),
                    //         "text"),
                    //     expandedRow(
                    //         textView("Sub Kategori", Alignment.centerRight),
                    //         "text"),
                    //   ],
                    // ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        expandedRow(buttonKategori, "button"),
                        expandedRow(buttonSubKategori, "button")
                      ],
                    ),
                    // Row(
                    //   mainAxisSize: MainAxisSize.max,
                    //   children: <Widget>[
                    //     expandedRow(
                    //         textView("Daop/Divre", Alignment.centerLeft),
                    //         "text"),
                    //     expandedRow(
                    //         textView("Stasiun", Alignment.centerRight),
                    //         "text"),
                    //   ],
                    // ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        expandedRow(buttonDaop, "button"),
                        expandedRow(buttonStasiun, "button")
                      ],
                    ),
                    // Row(
                    //   mainAxisSize: MainAxisSize.max,
                    //   children: <Widget>[
                    //     expandedRow(
                    //         textView("No Sarana", Alignment.center), "text"),
                    //   ],
                    // ),

                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        expandedRow(buttonNo, "button"),
                        expandedRow(_buttonDate(context), "button"),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        expandedRow(
                            textView("RELASI", Alignment.center), "text"),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        expandedRow(buttonRelasiAwal, "button"),
                        expandedRow(buttonRelasiAkhir, "button")
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        expandedRow(Center(child: fieldSarana), "textField"),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[expandedRow(fieldMitra, "textField")],
                    ),
                  ],
                ),
                // ),
                // ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 5.0,
                    // borderRadius: BorderRadius.circular(15.0),
                    color: Color(0xff01A0C7),
                    child: MaterialButton(
                        minWidth: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        onPressed: () {
                          _navigateToChecksheet(context);
                        },
                        child: Text(
                          "Submit",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        )),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Widget textView(String title, Alignment align) {
  final textWidget = new Align(
    alignment: align,
    child: new Text(
      title,
      style: TextStyle(color: Colors.white),
    ),
  );
  return textWidget;
}

Widget expandedRow(Widget widget, String type) {
  return Expanded(
    flex: 2,
    child: Container(
      // color: Colors.blue,
      child: new SizedBox(
        width: double.infinity,

        // height: double.infinity,
        child: Padding(
          padding: type == "text"
              ? const EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0)
              : const EdgeInsets.only(left: 8.0, right: 8.0),
          child: widget,
        ),
      ),
    ),
  );
}
