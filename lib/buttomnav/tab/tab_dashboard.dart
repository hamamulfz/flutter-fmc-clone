import 'package:flutter/material.dart';

class DashboardTab extends StatefulWidget {
  final BuildContext parentContext;
  DashboardTab({Key key, this.parentContext}) : super(key: key);

  @override
  _DashboardTabState createState() => _DashboardTabState();
}

class _DashboardTabState extends State<DashboardTab> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Dashboard"),
          backgroundColor: Colors.blue,
          actions: <Widget>[
          IconButton( 
            icon: Icon(Icons.exit_to_app),
            onPressed: () {Navigator.pop(widget.parentContext);},
          ),
          ],
        ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/back.png"),
            fit: BoxFit.cover,
            )
        ),
        child: null,
      ),
    );
  }
}