import 'package:flutter/material.dart';
import 'dart:convert';
import '../../report_detail.dart';
// import 'package:hello/main.dart';
// import 'dart:async';
// import 'package:flutter/services.dart';

class ReportTab extends StatelessWidget {
  final BuildContext parent;
  ReportTab({this.parent});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "List Search",
      debugShowCheckedModeBanner: false,
      
      // routes: <String, WidgetBuilder>{
      //   '/login': (BuildContext context) => new MyApp(),
      //   // '/screen3': (BuildContext context) => new Screen3(),
      //   // '/screen4': (BuildContext context) => new Screen4()
      // },
      home: MyReport(title: "List", parent: this.parent,),
    );
  }
}

class MyReport extends StatefulWidget {
  MyReport({Key key, this.title, this.parent}) : super(key: key);
  final String title;
  final BuildContext parent;
  @override
  _MyMyReportState createState() => _MyMyReportState();
}

class _MyMyReportState extends State<MyReport> {
  List data;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return new Scaffold(
      appBar: AppBar(
        title: Text("List Report"),
        backgroundColor: Colors.blue,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              Navigator.pop(widget.parent);
            },
          ),
        ],
      ),
      body: Stack(children: <Widget>[
        Center(
          child: new Image.asset(
            "assets/back.png",
            width: size.width,
            height: size.height,
            fit: BoxFit.fill,
          ),
        ),
        Container(
          child: FutureBuilder(
            future: DefaultAssetBundle.of(context)
                .loadString('assets/list_report.json'),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return CircularProgressIndicator();
              }
              var myData = json.decode(snapshot.data);
              var data = myData['data'];
              print(myData);
              return ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  var catRep = data[index]['category_name'];
                  var subCatRep = data[index]['sub_name'];
                  return new InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MyReportDetail(),
                        ),
                      );
                    },
                    child: Card(
                      color: Colors.transparent,
                      elevation: 0,
                      shape: new RoundedRectangleBorder(
                          side: new BorderSide(color: Colors.white, width: 2.0),
                          borderRadius: BorderRadius.circular(5.0)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Text(
                              data[index]['no_order'],
                              style: TextStyle(color: Colors.white),
                            ),
                            Text(
                              '$catRep - $subCatRep',
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: myData == null ? 0 : myData.length,
              );
            },
          ),
        ),
      ]),
    );
  }
}
