import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
// import './home.dart';
import 'package:hello/buttomnav/buttom_nav.dart';
// import 'package:hello/buttomnav/tab/tab_dashboard.dart';
// import 'package:hello/buttomnav/tab/tab_order.dart';
// import 'package:hello/buttomnav/tab/tab_report.dart';
// import 'package:hello/home.dart';
// import 'package:hello/report_detail.dart';
// import 'package:hello/search_list.dart';
import 'dart:convert';
// import 'dart:async' show Future;
// import 'package:flutter/services.dart' show rootBundle;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Login',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: <String, WidgetBuilder>{
        '/': (BuildContext context) => new MyHomePage(),
        '/navbar': (BuildContext context) => new ButtomNav(),
        
        // '/screen4': (BuildContext context) => new Screen4()
      },
      // home: new MyHomePage(title: 'Flutter Login'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  var jsonString =
      '{ "name": "Administrator", "password": "password", "picture": "no pic","token": "token","email": "admin@mail.com", "phone": "081212344321", "username": "admin", "jabatan": "Administrator"}';

  String errorLogin = "";

  void _failedToLogin(String message) {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      errorLogin = message;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    Map<String, dynamic> user = jsonDecode(jsonString);

    final logoKai = SizedBox(
      height: 155.0,
      child: Image.asset(
        "assets/logo.png",
        fit: BoxFit.contain,
      ),
    );

    final usernameField = TextField(
      obscureText: false,
      controller: usernameController,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Username",
          hintStyle: TextStyle(color: Colors.white),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final passwordField = TextField(
      obscureText: true,
      controller: passwordController,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          hintStyle: TextStyle(color: Colors.white),
          fillColor: Colors.white,
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final appName = Text(
      "Facility Mobile Controlling",
      style: TextStyle(
          color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
    );

    var errorTextWidget = Text(
      errorLogin,
      style: TextStyle(color: Colors.red),
    );

// user defined function
    void _showDialog() {
      // flutter defined function
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Login Failed"),
            content: new Text("Wrong Username or Password"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    void _loginFunc() {
      var usernameText = usernameController.text;
      var passworText = passwordController.text;

      if (usernameText == user['username'] && passworText == user['password']) {
        errorLogin = "";
        usernameController.text = "";
        passwordController.text = "";
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ButtomNav(
                    userData: user,
                  )),
        );
      } else if (usernameText.isEmpty || passworText.isEmpty) {
        _failedToLogin("username or password cannot be blank");
      } else {
        _showDialog();
        _failedToLogin("Wrong username or password");
        print('error=' + errorLogin);
      }
    }

    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () {
            // loadCrossword();
            _loginFunc();
            // print(user['username']);
            // print(user['password']);

            // print('textfield=' + usernameController.text);
            // print('textfield=' + passwordController.text);
          },
          child: Text(
            "Login",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          )),
    );

    return new Scaffold(
      // appBar: AppBar(
      //   title: Text(widget.title),
      // ),
      body: new Stack(
        children: <Widget>[
          new Center(
            child: new Image.asset(
              "assets/back.png",
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
          ),
          new Center(
            child: new SingleChildScrollView(
              child: new Container(
                // color: Colors.white,
                child: new Padding(
                  padding: const EdgeInsets.all(36.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      logoKai,
                      SizedBox(height: 25.00),
                      appName,
                      SizedBox(height: 25.00),
                      usernameField,
                      SizedBox(height: 25.00),
                      passwordField,
                      SizedBox(height: 35.0),
                      loginButton,
                      SizedBox(
                        height: 15.0,
                      ),
                      errorTextWidget,
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

// Future<String> _loadJsonAssets() async {
//   return await rootBundle.loadString('data_json/accout_data.json');
// }

// Future loadCrossword() async {
//   String json = await _loadJsonAssets();
//   print(json);
// }
