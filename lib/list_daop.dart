import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
// import 'package:flutter/semantics.dart';
import 'dart:convert';
// import 'package:flutter/services.dart' show rootBundle;

// void main() {
//   runApp(SearchList());
// }

// class SearchList extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: "List Search",
//       home: MySearchList(title: "List"),
//     );
//   }
// }

class MyDaopList extends StatefulWidget {
  MyDaopList({Key key, this.title, this.daop}) : super(key: key);
  final String title;
  final daop;
  @override
  _MyDaopListState createState() => _MyDaopListState();
}

class _MyDaopListState extends State<MyDaopList> {
  // MyApp({Key key, @required this.list}) : super(key: key);
  TextEditingController searchController = TextEditingController();

  // final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  var items;
  @override
  void initState() {
    // items.addAll(duplicateItems);
    super.initState();
  }

  void filterSearchResults(String query, var duplicateItems) {
    Map<String, dynamic> dummySearchList;
    dummySearchList.addAll(duplicateItems);
    print(duplicateItems);
    if (query.isNotEmpty) {
      var dummyListData ;
      var i=0;
          print(query);
      while (i < duplicateItems.length){
        if(duplicateItems[i]['category_name'].contains(query)){
          print(duplicateItems[i]['category_name']);
          dummyListData.add(duplicateItems[i]);
        }
      }
      // dummySearchList.forEach((item) {
      //   if (item.contains(query)) {
      //     dummyListData.add(item);
      //   }
      // });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("List Daop - Stasiun"),
      ),
      body: Container(
        // height: double.infinity,
        // width: double.infinity,
        child: FutureBuilder(
            future: DefaultAssetBundle.of(context)
                .loadString('assets/data_json/daop.json'),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              }
              var daop = json.decode(snapshot.data);
              items = daop;

              if(widget.daop!=null){
                items = items[widget.daop]['station'];
              }
              print(items);
              return Column(
                children: <Widget>[
                  // Padding(
                  //   padding: const EdgeInsets.all(8.0),
                  //   child: TextField(
                  //     onChanged: (value) {
                  //       filterSearchResults(value, category);
                  //     },
                  //     controller: searchController,
                  //     decoration: InputDecoration(
                  //         labelText: "Search",
                  //         hintText: "Search",
                  //         prefixIcon: Icon(Icons.search),
                  //         border: OutlineInputBorder(
                  //             borderRadius:
                  //                 BorderRadius.all(Radius.circular(25.0)))),
                  //   ),
                  // ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: items.length == 0 ? 0 : items.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          onTap: () {
                            // Close the screen and return "Yep!" as the result.
                            Navigator.pop(
                                context, items[index]);
                          },
                          title: widget.daop!=null ? Text('${items[index]['singk']} - ${items[index]['nama_stn']}'): Text('${items[index]['singkatan']} - ${items[index]['nama']}'),
                        );
                      },
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }
}
