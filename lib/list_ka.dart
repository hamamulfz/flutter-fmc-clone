import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
// import 'package:flutter/semantics.dart';
import 'dart:convert';
// import 'package:flutter/services.dart' show rootBundle;

// void main() {
//   runApp(SearchList());
// }

// class SearchList extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: "List Search",
//       home: MySearchList(title: "List"),
//     );
//   }
// }

class MyKaList extends StatefulWidget {
  MyKaList({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MyKaListState createState() => _MyKaListState();
}

class _MyKaListState extends State<MyKaList> {
  // MyApp({Key key, @required this.list}) : super(key: key);
  TextEditingController searchNameController = TextEditingController();
  TextEditingController searchNoController = TextEditingController();

  // final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  var items;
  @override
  void initState() {
    // items.addAll(duplicateItems);
    super.initState();
  }

  void filterSearchResults(String query, var duplicateItems) {
    Map dummySearchList;
    dummySearchList = duplicateItems;
    print("In Map");
    print(dummySearchList['data']);
    print(query);
    if (query.isNotEmpty) {
      var dummyListData;
      for (var newData in dummySearchList['data']) {
        if ((newData['ka_no']).contain(query)) {
          dummyListData.add(newData);
          print(newData['ka_no']);
        }
      }
      // dummySearchList.forEach((item) {
      //   if ((item['ka_nm']).contains(query)) {
      //     dummyListData.add(item);
      //   }
      // });
      print(dummyListData);

      // dummySearchList.map(f){
      //    if(duplicateItems[i]['ka_nm'].contains(query)){
      // dummyListData.add(duplicateItems[i]);
      // }
      // };
      // while (i < duplicateItems.length){
      //   if(duplicateItems[i]['ka_nm'].contains(query)){
      //     dummyListData.add(duplicateItems[i]);
      //   }
      // }
      // dummySearchList.forEach((item) {
      //   if (item.contains(query)) {
      //     dummyListData.add(item);
      //   }
      // });
      setState(() {
        items.clear();
        items = dummyListData;
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("List KA"),
      ),
      body: Container(
        // height: double.infinity,
        // width: double.infinity,
        child: FutureBuilder(
            future: DefaultAssetBundle.of(context)
                .loadString('assets/data_json/list_ka.json'),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              }
              Map ka = json.decode(snapshot.data);
              items = ka['data'];
              return Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        TextField(
                          onChanged: (value) {
                            // filterSearchResults(value, ka);
                            // print(value);
                          },
                          controller: searchNameController,
                          decoration: InputDecoration(
                              labelText: "Search",
                              hintText: "No KA",
                              prefixIcon: Icon(Icons.search),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(25.0)))),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Material(
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(30.0),
                            color: Color(0xff01A0C7),
                            child: MaterialButton(
                                minWidth: MediaQuery.of(context).size.width,
                                padding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                onPressed: () {
                                  // loadCrossword();
                                  // _loginFunc();
                                  // print(user['username']);
                                  // print(user['password']);
                                  filterSearchResults(
                                      searchNameController.text, ka);
                                  // print('textfield=' + usernameController.text);
                                  // print('textfield=' + passwordController.text);
                                },
                                child: Text(
                                  "Search",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                )),
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: items.length == 0 ? 0 : items.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          onTap: () {
                            // Close the screen and return "Yep!" as the result.
                            Navigator.pop(context, items[index]);
                          },
                          title: Text(
                              '${items[index]['ka_no']} - ${items[index]['ka_nm']}'),
                        );
                      },
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }
}
